'use strict';

var five = require('johnny-five');
var board = new five.Board();

// function analogTo256(val) {
//   return Math.ceil(val * 255 / 1023);

// }

board.on('ready', function() {
  console.log('Ready');

  var red = new five.Led('11');
  var yellow = new five.Led('10');

  // First Attemp: Triggering Pin.read API
  // var potentiometer = new five.Pin('A0');
  // five.Pin.read(potentiometer, function(err, state) {
  //   if (err) {
  //     throw err;
  //   }

  //   red.brightness(analogTo256(state));
  //   yellow.brightness(255 - analogTo256(state));
  // });

  // Second Attemp: Using Sensor API
  var potentiometer = new five.Sensor('A0');

  potentiometer.scale(0, 255).on('change', function() {
    red.brightness(this.value);
    yellow.brightness(255 - this.value);
  });

});
