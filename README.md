Zemoga has an Arduino Uno!
==========================

Play with it and load your schematics and test files here for all of us to learn.
To see the board schematics, install Fritzing App http://fritzing.org/download/

This Arduino has StandardFirmata loaded, so it's just plug and play.

Install npm dependencies (Johnny-Five) and run your node scripts, e.g.:

```
npm i && node led-api.js
```


