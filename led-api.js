'use strict';

var five = require('johnny-five');
var board = new five.Board();
var noop = function() {};

board.on('ready', function() {
  console.log('Ready!');

  var leds = new five.Leds([10, 11]);
  var apiFactory = function(name) {
    return function(color, timing, cb) {
      cb = cb || noop;
      if (color >= 0 && leds[color]) {
        leds[color][name](timing | 500, cb);
      }
    };
  };

  this.repl.inject({
    red: 0,
    yellow: 1,
    on: apiFactory('on'),
    off: apiFactory('off'),
    blink: apiFactory('blink'),
    stop: apiFactory('stop'),
    fadeIn: apiFactory('fadeIn'),
    fadeOut: apiFactory('fadeOut')
  });
});
